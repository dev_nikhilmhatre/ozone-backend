from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from Utilities.utils import get_object, get_queryset

from .models import CategoryModel
from .serializers import CategorySerializer_AC, CategorySerializerDel_A


# Get call
class CategoryViewList_C(APIView):
    def get(self, request, format=None):
        
        # Customer or Admin
        if request.user.is_anonymous:
            is_anonymous = True
        else:
            is_anonymous = False
            
        category = get_queryset(CategoryModel, is_anonymous)

        # use select_related to fetch all foreign key data.. 
        # it saves us from multiple data base hits thus selecting all data in one hit

        # category = category_list.select_related('brand')  # create joins
        # for i in category:
        #     print(i.brand.slug)  access different fields of model with . notation and filed name
        
        serializer = CategorySerializer_AC(category, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


# Post call
class CategoryViewCreate_A(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = CategorySerializer_AC(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Put call
class CategoryViewUpdate_A(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def put(self, request, id_pk, format=None):
        category = get_object(CategoryModel, id_pk)
        serializer = CategorySerializer_AC(category, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


# Delete call
class CategoryViewDelete_A(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    
    def put(self, request, id_pk, format=None):
        category = get_object(CategoryModel, id_pk)
        serializer = CategorySerializerDel_A(category, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)
