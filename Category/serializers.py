from rest_framework import serializers

from .models import CategoryModel


# Read, Insert, Update
class CategorySerializer_AC(serializers.ModelSerializer):
    class Meta:
        model = CategoryModel
        fields = ['id', 'name', 'is_disabled']
        read_only_fields = ['id', 'is_disabled']


# Delete
class CategorySerializerDel_A(serializers.ModelSerializer):
    class Meta:
        model = CategoryModel        
        fields = ['id', 'is_disabled']
        read_only_fields = ['id']
