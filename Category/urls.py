from django.urls import path

from .views import (CategoryViewCreate_A, CategoryViewDelete_A,
                    CategoryViewList_C, CategoryViewUpdate_A)

urlpatterns = [
    path('list-c/', CategoryViewList_C.as_view()),
    path('create-a/', CategoryViewCreate_A.as_view()),
    path('update-a/<int:id_pk>/', CategoryViewUpdate_A.as_view()),
    path('delete-a/<int:id_pk>/', CategoryViewDelete_A.as_view()),
]
