from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from rest_framework import permissions, status
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView

# Create your views here.

# Login
class SignIn(APIView):
    def post(self, request, format=None):
        try:
            username = request.data['username']
            password = request.data['password']
            user = authenticate(username=username, password=password)
            
            if user is not None:
                try:
                    token = Token.objects.get(user=user)
                except:
                    token = None
                
                if token is None:
                    token = Token.objects.create(user=user)

                login(request, user)

                return Response({"token":str(token), "login":True, "msg":"Login Sussessful"}, status=status.HTTP_200_OK)
            return Response({"token":'', "login":False, "msg":"Login Unsuccessful"},status=status.HTTP_404_NOT_FOUND)
        except Exception as ex:
            return Response({"token":'', "login":False, "msg":type(ex).__name__},status=status.HTTP_404_NOT_FOUND)


# Logout
class SignOut(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        try:
            token = Token.objects.get(user_id=request.user)
            if token is not None:
                logout(request)
                token.delete()
                return Response({"login": False, "msg":"Logout Successful"}, status=status.HTTP_200_OK)
        except Exception as ex:
            return Response({"login": False, "msg":type(ex).__name__},status=status.HTTP_404_NOT_FOUND)
