from django.db import models

from Client.models import ClientModel
from Product.models import ProductModel
from Flavour.models import FlavourModel


class BillingModel(models.Model):
    client = models.ForeignKey(ClientModel, on_delete=models.CASCADE, blank=True)
    balance = models.FloatField(default=0)
    note = models.CharField(max_length=250, null=True, blank=True)
    
    Created = models.DateTimeField(auto_now=True)
    Updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.client)


class ItemsModel(models.Model):
    bill = models.ForeignKey(BillingModel, on_delete=models.CASCADE, blank=True)
    item = models.ForeignKey(ProductModel, on_delete=models.CASCADE, blank=True)
    flavour = models.ForeignKey(FlavourModel, on_delete=models.CASCADE, blank=True)
    price = models.FloatField(default=0)
    discount_in_percentage = models.IntegerField(default=0)
    discount_in_rupees = models.FloatField(default=0)
    quantity = models.IntegerField(default=0)
    is_free = models.BooleanField(default=False)
    is_returned = models.BooleanField(default=False)

    Created = models.DateTimeField(auto_now=True)
    Updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id) + " " + str(self.item) + " " + str(self.bill) + " " + str(self.quantity) + " " + str(self.is_returned)
