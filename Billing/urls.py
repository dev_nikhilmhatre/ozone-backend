from django.urls import path

from .views import (BillCreateView, BillReturnItemView, BillViewList,
                    BillViewRetrive)

urlpatterns = [
    path('list/', BillViewList.as_view()),
    path('create/', BillCreateView.as_view()),
    path('retrive/<int:id_pk>/', BillViewRetrive.as_view()),
    path('return/<int:id_pk>/', BillReturnItemView.as_view())
]
