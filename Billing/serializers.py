from rest_framework import serializers

from Client.serializers import ClientSerializer_A
from Flavour.serializers import FlavourSerializer_AC
from Product.serializers import ProductSerializer

from .models import BillingModel, ItemsModel


# Read
class BillingSerializerDML(serializers.ModelSerializer):
    client = ClientSerializer_A(read_only=True)
    class Meta:
        model = BillingModel
        fields = "__all__"


# Insert, Update
class BillingSerializerDDL(serializers.ModelSerializer):
    class Meta:
        model = BillingModel
        fields = "__all__"


# Read
class ItemsSerializerDML(serializers.ModelSerializer):
    bill = BillingSerializerDML(read_only=True)
    item = ProductSerializer(read_only=True)
    flavour = FlavourSerializer_AC(read_only=True)
    class Meta:
        model = ItemsModel
        fields = "__all__"
        # depth = 2 -> causes multiple queries in database.. avoid using this


# Insert, Update
class ItemsSerializerDDL(serializers.ModelSerializer):
    class Meta:
        model = ItemsModel
        fields = "__all__"


# Delete
class ItemsSerializerDel(serializers.ModelSerializer):
    class Meta:
        model = ItemsModel
        fields = ['id', 'is_returned']
        read_only_fields = ['id']
