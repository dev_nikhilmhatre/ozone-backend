from django.contrib import admin

from .models import BillingModel, ItemsModel

# Register your models here.
admin.site.register(BillingModel)
admin.site.register(ItemsModel)
