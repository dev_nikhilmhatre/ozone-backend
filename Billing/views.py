from django.db import transaction
from django.forms.models import model_to_dict
from django.http import Http404
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from Client.models import ClientModel
from Product.models import ProductModel
from Utilities.utils import get_object

from .models import BillingModel, ItemsModel
from .serializers import (BillingSerializerDDL, BillingSerializerDML,
                          ItemsSerializerDDL, ItemsSerializerDel,
                          ItemsSerializerDML)


# Get call (Get bill)
class BillViewList(APIView):
    def get(self, request):
        bills = BillingModel.objects.all().select_related('client')
        serializer = BillingSerializerDML(bills, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


# Post call (Create Bill)
class BillCreateView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        client = request.data['client']
        
        with transaction.atomic():
            #  balance code
            clientInstance = ClientModel.objects.get(pk=client['client'])
            clientInstance.balance += float(client['balance'])
            clientInstance.save()

            items = request.data['items']
            serializer_bill = BillingSerializerDDL(data=client)
            if serializer_bill.is_valid():
                serializer_bill.save()
                for item in items:
                    item['bill'] = serializer_bill.data['id']
                    try:
                        product = ProductModel.objects.get(pk=item['item'])
                        product.quantity = product.quantity - item['quantity']
                        product.save()
                    except ProductModel.DoesNotExist:
                        raise Http404
                serializer_items = ItemsSerializerDDL(data=items, many=True)
                if serializer_items.is_valid():
                    serializer_items.save()
                    return Response([serializer_bill.data, serializer_items.data], status=status.HTTP_201_CREATED)
        return Response([serializer_bill.errors, serializer_items.errors], status=status.HTTP_400_BAD_REQUEST)


# Retrive call
class BillViewRetrive(APIView):
    def get(self, request, id_pk, format=None):
        items = ItemsModel.objects.filter(bill=id_pk).select_related(
            'bill',
            'item',
            'flavour',
            'bill__client',
            'item__brand',
            'item__category',
        )
        serializer_items = ItemsSerializerDML(items, many=True)
        return Response(serializer_items.data, status=status.HTTP_200_OK)


# Put call (Return item)
class BillReturnItemView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    
    def put(self, request, id_pk):
        data = request.data
        with transaction.atomic():
            try:
                item = ItemsModel.objects.prefetch_related('item').get(pk=id_pk)

                product = ProductModel.objects.get(pk=item.item.id)
                product.quantity += int(data['quantity'])
                product.save()
                
                #  balance code
                clientInstance = ClientModel.objects.get(pk=item.bill.client.id)
                clientInstance.balance += float(data['quantity']) * item.price
                clientInstance.save()
                
                if data['is_all'] == "true":
                    serializer = ItemsSerializerDel(item, data={"is_returned": True})
                    if serializer.is_valid():
                        serializer.save()
                        return Response(serializer.data, status=status.HTTP_200_OK)
                else:
                    item.quantity -= int(data['quantity'])
                    item.save()

                    item.quantity = data['quantity']
                    item.is_returned = True
                    item = model_to_dict(item)
                    del item['id']
                    serializer = ItemsSerializerDDL(data=item)
                    if serializer.is_valid():
                        serializer.save()
                        return Response(serializer.data, status=status.HTTP_200_OK)
                    
            except ItemsModel.DoesNotExist:
                raise Http404
            except ProductModel.DoesNotExist:
                raise Http404
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
