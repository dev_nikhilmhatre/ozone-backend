# Generated by Django 2.1 on 2018-08-19 11:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ClientModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=150)),
                ('contact', models.CharField(blank=True, max_length=15)),
                ('email', models.EmailField(blank=True, max_length=254)),
                ('address', models.TextField(blank=True, null=True)),
                ('balance', models.FloatField(default=0)),
                ('Created', models.DateTimeField(auto_now=True)),
                ('Updated', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
