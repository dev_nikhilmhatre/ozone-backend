from rest_framework import serializers

from .models import ClientModel


# Read, Insert, Update, Delete
class ClientSerializer_A(serializers.ModelSerializer):
    class Meta:
        model = ClientModel
        fields = "__all__"
