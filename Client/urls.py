from django.urls import path

from .views import ClientListCreateView_A, ClientRetrieveUpdateDestroyView_A

urlpatterns = [
    path('list-create-a/',ClientListCreateView_A.as_view()),
    path('retrive-update-destroy-a/<int:pk>/',ClientRetrieveUpdateDestroyView_A.as_view()),
]
