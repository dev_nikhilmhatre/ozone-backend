from rest_framework import permissions
from rest_framework.generics import (ListCreateAPIView,
                                     RetrieveUpdateDestroyAPIView)

from .models import ClientModel
from .serializers import ClientSerializer_A


# get and post call
class ClientListCreateView_A(ListCreateAPIView):
    queryset = ClientModel.objects.all()
    serializer_class = ClientSerializer_A
    permission_classes = (permissions.IsAuthenticated,)


# get put and delete call
class ClientRetrieveUpdateDestroyView_A(RetrieveUpdateDestroyAPIView):
    queryset = ClientModel.objects.all()
    serializer_class = ClientSerializer_A
    permission_classes = (permissions.IsAuthenticated,)
