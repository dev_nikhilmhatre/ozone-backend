from django.db import models


class ClientModel(models.Model):
    name = models.CharField(max_length=150, blank=True)
    contact = models.CharField(max_length=15, blank=True)
    email = models.EmailField(blank=True)
    address = models.TextField(null=True, blank=True)
    balance = models.FloatField(default=0)

    Created = models.DateTimeField(auto_now=True)
    Updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
