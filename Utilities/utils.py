import random
import string
from django.http import Http404

def random_string_generator(size=10, chars=string.ascii_letters):
    return ''.join(random.choice(chars) for _ in range(size))


def unique_slug_generator(instance, new_slug=None):
    if new_slug is not None:
        slug = new_slug
    else:
        slug = random_string_generator()
    Klass = instance.__class__
    qs_exists = Klass.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
            slug=slug,
            randstr=random_string_generator(size=6)
        )
        return unique_slug_generator(instance, new_slug=new_slug)
    return slug


def generate_slug(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


def get_queryset(model, is_anonymous):
    if is_anonymous:
        return model.objects.filter(is_disabled=False).order_by('-name')
    else:
        return model.objects.all().order_by('-Updated')


def get_object(model,id_pk):
    try:
        return model.objects.get(pk=id_pk)
    except model.DoesNotExist:
        raise Http404


def pagination(queryset, page=0):
    start = page * 9
    end = start + 9
    return queryset[start:end]