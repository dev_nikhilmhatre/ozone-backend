from django.db import models


class FlavourModel(models.Model):
    name = models.CharField(max_length=100, blank=True)

    is_disabled = models.BooleanField(default=False)
    Created = models.DateTimeField(auto_now=True)
    Updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name