from django.contrib import admin

from .models import FlavourModel

# Register your models here.
admin.site.register(FlavourModel)
