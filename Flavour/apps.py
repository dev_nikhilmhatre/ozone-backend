from django.apps import AppConfig


class FlavourConfig(AppConfig):
    name = 'Flavour'
