from rest_framework import serializers

from .models import FlavourModel


# Read, Insert, Update
class FlavourSerializer_AC(serializers.ModelSerializer):
    class Meta:
        model = FlavourModel
        fields = ['id', 'name', 'is_disabled']
        read_only_fields = ['id', 'is_disabled']


#  Delete
class FlavourSerializerDel_A(serializers.ModelSerializer):
    class Meta:
        model = FlavourModel
        fields = ['id', 'is_disabled']
        read_only_fields = ['id']
