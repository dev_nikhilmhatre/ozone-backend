from django.urls import path

from .views import (FlavourViewCreate_A, FlavourViewDelete_A,
                    FlavourViewList_C, FlavourViewUpdate_A)

urlpatterns = [
    path('list-c/', FlavourViewList_C.as_view()),
    path('create-a/', FlavourViewCreate_A.as_view()),
    path('update-a/<int:id_pk>/', FlavourViewUpdate_A.as_view()),
    path('delete-a/<int:id_pk>/', FlavourViewDelete_A.as_view()),

    # path('search/', FlavourViewSearch.as_view())
]
