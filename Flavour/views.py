from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from Utilities.utils import get_object, get_queryset

from .models import FlavourModel
from .serializers import FlavourSerializer_AC, FlavourSerializerDel_A


# Get call
class FlavourViewList_C(APIView):
    def get(self, request, format=None):
        # Customer or Admin
        if request.user.is_anonymous:
            is_anonymous = True
        else:
            is_anonymous = False
            
        flavours = get_queryset(FlavourModel, is_anonymous)
        serializer = FlavourSerializer_AC(flavours, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


# Post call
class FlavourViewCreate_A(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = FlavourSerializer_AC(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# Put call
class FlavourViewUpdate_A(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def put(self, request, id_pk, format=None):
        flavour = get_object(FlavourModel, id_pk)
        serializer = FlavourSerializer_AC(flavour, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


# Delete call
class FlavourViewDelete_A(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    
    def put(self, request, id_pk, format=None):
        flavour = get_object(FlavourModel, id_pk)
        serializer = FlavourSerializerDel_A(flavour, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)
