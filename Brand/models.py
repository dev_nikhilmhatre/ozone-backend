from django.db import models

class BrandModel(models.Model):
    countries = (
        ("India", "India"),
        ("Other", "Other")
    )
    name = models.CharField(max_length=100, blank=True)
    country = models.CharField(choices=countries, max_length=6, null=True, blank=True)

    is_disabled = models.BooleanField(default=False)
    Created = models.DateTimeField(auto_now=True)
    Updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name