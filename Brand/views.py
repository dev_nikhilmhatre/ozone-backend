from django.db.models import Q
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from Utilities.utils import get_object, get_queryset

from .models import BrandModel
from .serializers import BrandSerializer_AC, BrandSerializerDel_A


# Get call
class BrandViewList_C(APIView):
    def get(self, request, format=None):
        # Customer or Admin
        if request.user.is_anonymous:
            is_anonymous = True
        else:
            is_anonymous = False
            
        brands = get_queryset(BrandModel, is_anonymous)
        serializer = BrandSerializer_AC(brands, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


# Post call
class BrandViewCreate_A(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        serializer = BrandSerializer_AC(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# Put call
class BrandViewUpdate_A(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def put(self, request, id_pk, format=None):
        brand = get_object(BrandModel, id_pk)
        serializer = BrandSerializer_AC(brand, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


# Delete call
class BrandViewDelete_A(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    
    def put(self, request, id_pk, format=None):
        brand = get_object(BrandModel, id_pk)
        serializer = BrandSerializerDel_A(brand, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)
