from rest_framework import serializers

from .models import BrandModel


# Read, Insert, Update
class BrandSerializer_AC(serializers.ModelSerializer):
    class Meta:
        model = BrandModel
        fields = ['id', 'name', 'country', 'is_disabled']
        read_only_fields = ['id', 'is_disabled']


# Delete
class BrandSerializerDel_A(serializers.ModelSerializer):
    class Meta:
        model = BrandModel
        fields = ['id', 'is_disabled']
        read_only_fields = ['id']
