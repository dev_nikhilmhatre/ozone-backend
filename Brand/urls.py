from django.urls import path

from .views import (BrandViewCreate_A, BrandViewDelete_A, BrandViewList_C,
                    BrandViewUpdate_A)

urlpatterns = [
    path('list-c/', BrandViewList_C.as_view()),
    path('create-a/', BrandViewCreate_A.as_view()),
    path('update-a/<int:id_pk>/', BrandViewUpdate_A.as_view()),
    path('delete-a/<int:id_pk>/', BrandViewDelete_A.as_view()),
]
