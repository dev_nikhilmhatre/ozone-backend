from rest_framework import serializers

from Brand.serializers import BrandSerializer_AC
from Category.serializers import CategorySerializer_AC
from Flavour.serializers import FlavourSerializer_AC

from .models import ( MediaModel, ProductInfoModel,
                     ProductModel, QuantityModel)

# ============================================= Product ==========================================================

# Add product 
class ProductAddUpdate(serializers.ModelSerializer): 
    class Meta:
        model = ProductModel
        fields = "__all__"


class ProductInfoAddUpdate(serializers.ModelSerializer): 
    class Meta:
        model = ProductInfoModel
        fields = "__all__"



class QuantityAddUpdate(serializers.ModelSerializer): 
    class Meta:
        model = QuantityModel
        fields = "__all__"


# --------------------- Test Code ---------------------

class QuantityRetrive_A(serializers.ModelSerializer):
    class Meta:
        model = QuantityModel
        fields = ['flavours', 'quantity', 'images', 'product_info', 'id']

class ProductInfoRetrive_A(serializers.ModelSerializer):
    quantitymodel_set = QuantityRetrive_A(read_only=True, many=True)
    class Meta:
        model = ProductInfoModel
        fields = ['purchase_price', 'mrp', 'discount', 'weight_lbs', 'quantitymodel_set', 'product', 'id']


class ProductRetrive_A(serializers.ModelSerializer):
    productinfomodel_set = ProductInfoRetrive_A(read_only=True, many=True)
    class Meta:
        model = ProductModel
        fields = [
            'id',
            'name',
            'brand',
            'category',
            'key_features',
            'is_non_veg',
            'description',
            'productinfomodel_set'
            ]

# --------------------- Test Code ---------------------


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


# Read Info (Display all product on Client)

class PS_Read(serializers.ModelSerializer):  
    class Meta:
        model = ProductModel
        fields = ['id', 'name', 'is_disabled']

class PIS_Read(serializers.ModelSerializer):  
    product = PS_Read(read_only=True)
    class Meta:
        model = ProductInfoModel
        fields = ['id', 'product', 'mrp', 'discount', 'weight_lbs']

class QS_Read(serializers.ModelSerializer): 
    flavours = FlavourSerializer_AC(read_only=True)
    product_info = PIS_Read(read_only=True)
    class Meta:
        model = QuantityModel
        fields = ['id','images', 'product_info', 'flavours']

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


# Read Info (Display single product on Client)

# class KFS_Retrive(serializers.ModelSerializer): 
#     class Meta:
#         model = KeyFeaturesModel
#         fields = ['id', 'name']

class PS_Retrive(serializers.ModelSerializer): 
    brand = BrandSerializer_AC(read_only=True)
    # key_features = KFS_Retrive(many=True, read_only=True)
    class Meta:
        model = ProductModel
        fields = ['name', 'brand', 'key_features', 'is_non_veg', 'description']

class PIS_Retrive(serializers.ModelSerializer): 
    product = PS_Retrive(read_only=True)
    class Meta:
        model = ProductInfoModel
        fields = ['id', 'product', 'mrp', 'discount', 'weight_lbs']


class QS_Retrive(serializers.ModelSerializer): 
    product_info = PIS_Retrive(read_only=True)
    flavours = FlavourSerializer_AC(read_only=True)
    class Meta:
        model = QuantityModel
        fields = ['images', 'product_info','flavours', 'quantity']


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


# Extra Links
class PS_Links(serializers.ModelSerializer): 
    class Meta:
        model = ProductModel
        fields = ['id', 'name']

class PIS_Links(serializers.ModelSerializer): 
    product = PS_Links(read_only=True)
    class Meta:
        model = ProductInfoModel
        fields = ['id', 'product', 'weight_lbs', 'discount']

class QS_Links(serializers.ModelSerializer): 
    product_info = PIS_Links(read_only=True)
    flavours = FlavourSerializer_AC(read_only=True)
    class Meta:
        model = QuantityModel
        fields = ['id','product_info','flavours', 'quantity']


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


# product list for admin
class ProductsListSerializer_A(serializers.ModelSerializer):   
    productinfomodel_set = PIS_Read(read_only=True, many=True)
    class Meta:
        model = ProductModel
        fields = ['id','name', 'is_disabled', 'productinfomodel_set']



# ================================== Key Features Serializer ============================================

# # Read, Insert, Update
# class KeyFeaturesSerializer_AC(serializers.ModelSerializer): 
#     class Meta:
#         model = KeyFeaturesModel
#         fields = ['id', 'name', 'is_disabled']
#         read_only_fields = ['id']


# # Delete
# class KeyFeaturesSerializerDel_A(serializers.ModelSerializer): 
#     class Meta:
#         model = KeyFeaturesModel
#         fields = ['id', 'is_disabled']
#         read_only_fields = ['id']



# ==================================== Billing ==========================================================


# For Billing model
class ProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer_AC(read_only=True)
    brand = BrandSerializer_AC(read_only=True)
    class Meta:
        model = ProductModel
        exclude = ("flavours",)


# ======================================= Media ==========================================================

class MediaSerializer(serializers.ModelSerializer): 
    class Meta:
        model = MediaModel
        fields = '__all__'



# ======================================================================================================

# 1) data from foreign key tabel (only works with foreign key table)
# field_name = serializers.CharField(source="foreignKeyFieldName.foreignTableField", read_only=True) 

# 2) name is writable only.. it will not be included in o/p
# extra_kwargs = {'name': {'write_only': True}} 

# 3) Specify those fields which should no update
# read_only_fields = ()

# 4) Specify fields those not needed in o/p.. non writable and readable
# exclude = ()

# 5) get data from foreign key  and manytomany relation -> Expensive Query
# depth will decide internal joins i.e. table = fk table -> fk table and so on
# depth = 1

# ======================================================================================================

#  Practice 
# class Test3(serializers.ModelSerializer):
#     class Meta:
#         model = KeyFeaturesModel
#         fields = "__all__"

# class Test1(serializers.ModelSerializer):
#     key_features = Test3(many=True)
#     brand = BrandSerializer(read_only=True)
#     class Meta:
#         model = ProductModel
#         fields = "__all__"

# class Test2(serializers.ModelSerializer):
    
#     product = Test1(read_only=True)
#     flavours = FlavourSerializer(many=True)
#     class Meta:
#         model = ProductInfoModel
#         fields = [ 'product', 'flavours']
