from django.contrib.postgres.fields import ArrayField
from django.db import models

from Brand.models import BrandModel
from Category.models import CategoryModel
from Flavour.models import FlavourModel

# from PIL import Image as Img

# class KeyFeaturesModel(models.Model):
#     name = models.CharField(max_length=150)

#     is_disabled = models.BooleanField(default=False)
#     Created = models.DateTimeField(auto_now=True)
#     Updated = models.DateTimeField(auto_now_add=True)

#     def __str__(self):
#         return self.name


class ProductModel(models.Model):
    brand = models.ForeignKey(BrandModel, on_delete=models.CASCADE, null=True, blank=True)
    category = models.ForeignKey(CategoryModel, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=100, blank=True,  null=True)
    key_features = models.TextField(null=True, blank=True)
    is_non_veg = models.BooleanField(default=False)
    description = models.TextField(null=True, blank=True)

    is_disabled = models.BooleanField(default=False)
    Created = models.DateTimeField(auto_now=True)
    Updated = models.DateTimeField(auto_now_add=True)

    # def __str__(self):
    #     return self.name


class ProductInfoModel(models.Model):
    product = models.ForeignKey(ProductModel, on_delete=models.CASCADE)
    purchase_price = models.FloatField(default=0.0)
    mrp = models.FloatField(default=0.0)
    discount = models.IntegerField(default=0)
    weight_lbs = models.DecimalField(default=0, decimal_places=2, max_digits=6)
    
    is_disabled = models.BooleanField(default=False)
    Created = models.DateTimeField(auto_now=True)
    Updated = models.DateTimeField(auto_now_add=True)

    # def __str__(self):
    #     return str(self.id)


class QuantityModel(models.Model):
    product_info = models.ForeignKey(ProductInfoModel, on_delete=models.CASCADE, null=True, blank=True)
    flavours = models.ForeignKey(FlavourModel, on_delete=models.CASCADE, null=True, blank=True)
    images = ArrayField(models.TextField(blank=True), default=list)
    quantity = models.IntegerField(default=0)
    views = models.IntegerField(default=0)

    is_disabled = models.BooleanField(default=False)
    Created = models.DateTimeField(auto_now=True)
    Updated = models.DateTimeField(auto_now_add=True)

    # def __str__(self):
    #     return str(self.id)

# Compress and save image

# if self.image:
#     img = Img.open(self.image)
#     width, height = img.size
#     print(width, height)
#     img = img.resize((width,height),Img.ANTIALIAS)
#     img.save('ozone/media/images/testImage.jpg',optimize=True,quality=50)

class MediaModel(models.Model):
    image= models.ImageField(upload_to="images/")
