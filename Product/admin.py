from django.contrib import admin

from .models import MediaModel, ProductInfoModel, ProductModel, QuantityModel

# Register your models here.
admin.site.register(ProductModel)
admin.site.register(ProductInfoModel)
# admin.site.register(KeyFeaturesModel)
admin.site.register(QuantityModel)
admin.site.register(MediaModel)
