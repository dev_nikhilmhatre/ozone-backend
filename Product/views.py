from django.db import transaction
from django.db.models import Q
from django.forms.models import model_to_dict
from django.http import Http404
from rest_framework import parsers, permissions, status
from rest_framework.generics import (ListCreateAPIView,
                                     RetrieveUpdateDestroyAPIView)
from rest_framework.response import Response
from rest_framework.views import APIView
from Utilities.utils import get_object, get_queryset, pagination

from .models import ProductInfoModel, ProductModel, QuantityModel
from .serializers import (MediaSerializer, ProductAddUpdate,
                          ProductInfoAddUpdate, ProductRetrive_A,
                          ProductsListSerializer_A, QS_Links, QS_Read,
                          QS_Retrive, QuantityAddUpdate)

# -----------------------------------------------------------------------------------------------

#  Get Call (Display all product on client)
class ProductViewList_C(APIView):
    def get(self, request, format=None):
        # Customer or Admin
        if request.user.is_anonymous:
            is_anonymous = True
        else:
            is_anonymous = False

        # pagination
        try:
            page = int(request.GET.get('page'))
        except:
            page = 0
        
        quantities = get_queryset(QuantityModel, is_anonymous)
        quantities = quantities.select_related(
            'product_info',
            'product_info__product',
            'flavours'
            ).filter(
                product_info__product__brand__is_disabled=False,
                product_info__product__category__is_disabled=False,
                flavours__is_disabled=False,
                quantity__gt=0
                ).order_by('-views')
        quantities = pagination(quantities, page)

        count = QuantityModel.objects.filter(
            product_info__product__brand__is_disabled=False,
            product_info__product__category__is_disabled=False,
            flavours__is_disabled=False,
            quantity__gt=0
            ).count()

        serializer =  QS_Read(quantities, many=True)
        
        return Response([serializer.data, count], status=status.HTTP_200_OK)


# Retrive call (Display single product on client)
class ProductViewRetrive_C(APIView):
    def get(self, request, id_pk, pk, format=None):
        try:
            product = QuantityModel.objects.select_related(
                    'product_info', 
                    'flavours', 
                    'product_info__product',
                    'product_info__product__brand',
                    ).get(pk=id_pk)
        except QuantityModel.DoesNotExist:
            raise Http404
        

        links = QuantityModel.objects.select_related(
                                    'product_info', 
                                    'flavours', 
                                    'product_info__product',
                                    ).filter(product_info__product=pk).filter(~Q(pk=id_pk))
                                    
        
        serializer = QS_Retrive(product)
        serializer_links = QS_Links(links, many=True)
        
        return Response([serializer.data, serializer_links.data], status=status.HTTP_200_OK)


# Search call
class ProductViewSearch_C(APIView):
    def post(self, request, format=None):

        data = request.data

        # get entire query string and convert it into dictionary:

        # search_query = dict(QueryDict(request.GET.urlencode()))
        # search_query = request.GET.get('q')

        try:
            price = float(data['mrp'])
        except:
            price = ''
        
        try:
            brands = list(data['brands'])
        except:
            brands = []
        
        try:
            category = list(data['category'])
        except:
            category = []
        
        try:
            search = str(data['search'])
        except:
            search = ''
        
        try:
            flavours = list(data['flavours'])
        except:
            category = []

        sort =  data['sorting']
        other = data['other']
        
        quantities = QuantityModel.objects.filter(is_disabled=False)
        
        if (other != 'all'):
            quantities = quantities.filter(product_info__product__brand__country=other)
            print(quantities)

        if (search != ''):
            quantities = quantities.filter(
                                        Q(product_info__product__name__iexact=search) |
                                        Q(product_info__product__name__icontains=search) |
                                        Q(product_info__product__brand__name__iexact=search) |
                                        Q(product_info__product__brand__name__icontains=search) |
                                        Q(flavours__name__iexact=search) |
                                        Q(flavours__name__icontains=search)
                                        )

        if (price != ''):
            quantities = quantities.filter(
                                        product_info__mrp__lte=price
                                        ).order_by('-product_info__mrp')
        
        if (len(brands) > 0):
            quantities = quantities.filter(product_info__product__brand__in=brands)
        

        if (len(category) > 0):
            quantities = quantities.filter(product_info__product__category__in=category)

        
        if (len(flavours) > 0):
            quantities = quantities.filter(flavours__in=flavours)


        quantities = quantities.select_related(
            'product_info',
            'product_info__product'
            ).filter(
                product_info__product__brand__is_disabled=False,
                product_info__product__category__is_disabled=False,
                flavours__is_disabled=False,
                quantity__gt=0
                ).order_by(sort)
        
        # add pagination
        # Load more button needs update
        serializer = QS_Read(quantities, many=True)
        
        return Response([serializer.data, len(serializer.data)], status=status.HTTP_200_OK)


# Post call (Admin)
class ProductViewCreate_A(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        api_data = request.data
            
        product_data = {}
        # product_info_data= []
        # quantity_data= []

        try:
            with transaction.atomic():
                product_data['brand'] = api_data['brand']
                product_data['category'] = api_data['category']
                product_data['name'] = api_data['productName']
                product_data['key_features'] = api_data['key_features']
                product_data['is_non_veg'] = api_data['nonVeg']
                product_data['description'] = api_data['description']

                product = ProductAddUpdate(data=product_data)
                if product.is_valid():
                    product.save()

                wtData = api_data['weightDetail']
                for wt in wtData:
                    tempWt = {
                        'product':product.data['id'],
                        'purchase_price':wtData[wt]['purchasePrice'],
                        'mrp':wtData[wt]['mrp'],
                        'discount':wtData[wt]['discount'],
                        'weight_lbs':wtData[wt]['weight']
                    }
                    
                    # product_info_data.append(tempWt)

                    info = ProductInfoAddUpdate(data=tempWt)
                    if info.is_valid():
                        info.save()
                    
                    flvData = wtData[wt]['flvDetails']
                    for flv in flvData:
                        tempFlv = {
                            'product_info':info.data['id'],
                            'flavours':flvData[flv]['flavour'],
                            'quantity':flvData[flv]['quantity']
                        }
                        
                        imgs = []
                        for img in flvData[flv]['images']:
                            imgs.append(img['image'])
                        tempFlv['images'] = imgs

                        # quantity_data.append(tempFlv)

                        flavour = QuantityAddUpdate(data=tempFlv)
                        if flavour.is_valid():
                            flavour.save()

            return Response({"Error":False, "id":product.data['id']}, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({"Error":True}, status=status.HTTP_400_BAD_REQUEST)


# Post call (Admin)
class ProductViewUpdate_A(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def put(self, request, id_pk, format=None):
        api_data = request.data
        
        product_data = {}
        # product_info_data= []
        # quantity_data= []

        try:
            with transaction.atomic():
                product_data['brand'] = api_data['brand']
                product_data['category'] = api_data['category']
                product_data['name'] = api_data['productName']
                product_data['key_features'] = api_data['key_features']
                product_data['is_non_veg'] = api_data['nonVeg']
                product_data['description'] = api_data['description']
                
                product = ProductModel.objects.get(pk=id_pk)
                product = ProductAddUpdate(product, data=product_data)
                if product.is_valid():
                    product.save()

                wtData = api_data['weightDetail']
                
                for wt in wtData:

                    tempWt = {
                        'product':product.data['id'],
                        'purchase_price':wtData[wt]['purchasePrice'],
                        'mrp':wtData[wt]['mrp'],
                        'discount':wtData[wt]['discount'],
                        'weight_lbs':wtData[wt]['weight']
                    }
                    
                    # product_info_data.append(tempWt)
                    try:
                        info = ProductInfoModel.objects.get(pk=wtData[wt]['id'])
                        info = ProductInfoAddUpdate(info, data=tempWt)
                    except Exception as e:
                        info = ProductInfoAddUpdate(data=tempWt)
                    if info.is_valid():
                        info.save()
                    
                    flvData = wtData[wt]['flvDetails']
                    for flv in flvData:
                        tempFlv = {
                            'product_info':info.data['id'],
                            'flavours':flvData[flv]['flavour'],
                            'quantity':flvData[flv]['quantity']
                        }
                        
                        imgs = []
                        for img in flvData[flv]['images']:
                            if(not isinstance(img, str)):
                                imgs.append(img['image'])
                            else:
                                imgs.append(img)
                        tempFlv['images'] = imgs
                        
                        # quantity_data.append(tempFlv)

                        try:
                            flavour = QuantityModel.objects.get(pk=flvData[flv]['id'])
                            flavour = QuantityAddUpdate(flavour, data=tempFlv)
                        except Exception as e:
                            flavour = QuantityAddUpdate(data=tempFlv)
                        
                        if flavour.is_valid():
                            flavour.save()

            return Response({"Error":False, "id":id_pk}, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({"Error":True}, status=status.HTTP_400_BAD_REQUEST)


# (Admin)
class GetProductList_A(APIView):
    def get(self, request, format=None):
        if request.user.is_anonymous:
            is_anonymous = True
        else:
            is_anonymous = False
        products = get_queryset(ProductModel, is_anonymous)

        products = products.prefetch_related(
            'productinfomodel_set'
            )
        
        serializer = ProductsListSerializer_A(products, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)



class GetSingleProduct_A(APIView):
    def get(self, request, id_pk, format=None):
        data = ProductModel.objects.prefetch_related('productinfomodel_set', 'productinfomodel_set__quantitymodel_set').get(pk=id_pk)
        serializer = ProductRetrive_A(data)
        return Response(serializer.data, status=status.HTTP_200_OK)


class MediaView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    
    def post(self, request, format=None):
        serializer = MediaSerializer(data={'image':request.data.getlist('image')[0]})
        if serializer.is_valid():
            serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)


# ----------------------------Key Features-----------------------------------------------


# # Get call
# class KeyFeaturesViewList_A(APIView):
#     def get(self, request, format=None):
#         # Customer or Admin
#         if request.user.is_anonymous:
#             is_anonymous = True
#         else:
#             is_anonymous = False
        
#         key_features =  get_queryset(KeyFeaturesModel, is_anonymous)
#         # KeyFeaturesModel.objects.all()
#         serializer = KeyFeaturesSerializer_AC(key_features, many=True)
#         return Response(serializer.data, status=status.HTTP_200_OK)

# # Post call
# class KeyFeaturesViewCreate_A(APIView):
#     permission_classes = (permissions.IsAuthenticated,)

#     def post(self, request, format=None):
#         serializer = KeyFeaturesSerializer_AC(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# # Put call
# class KeyFeaturesViewUpdate_A(APIView):
#     permission_classes = (permissions.IsAuthenticated,)

#     def put(self, request, id_pk, format=None):
#         key_feature = get_object(KeyFeaturesModel, id_pk)
#         serializer = KeyFeaturesSerializer_AC(key_feature, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_200_OK)
#         return Response(status=status.HTTP_400_BAD_REQUEST)


# # Delete call
# class KeyFeaturesViewDelete_A(APIView):
#     permission_classes = (permissions.IsAuthenticated,)
    
#     def put(self, request, id_pk, format=None):
#         key_feature = get_object(KeyFeaturesModel, id_pk)
#         serializer = KeyFeaturesSerializerDel_A(key_feature, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_200_OK)
#         return Response(status=status.HTTP_400_BAD_REQUEST)


# -----------------------------------------------------------------------------------------------



# Practice
# class TestApi(APIView):
#     def get(self, request, id_pk, format=None):
#         from django.db.models import Prefetch
#         try:
#             product = ProductInfoModel.objects.select_related('product', 'product__brand').prefetch_related('flavours', 'product__key_features').get(pk=id_pk)
#         except ProductInfoModel.DoesNotExist:
#             raise Http404
        
#         serializer = Test2(product)

#         return Response(serializer.data, status=status.HTTP_200_OK)
