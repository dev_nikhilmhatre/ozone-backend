from django.urls import path

from .views import (GetProductList_A, GetSingleProduct_A, MediaView,
                    ProductViewCreate_A, ProductViewList_C,
                    ProductViewRetrive_C, ProductViewSearch_C,
                    ProductViewUpdate_A)

urlpatterns = [
    path('list-c/', ProductViewList_C.as_view()), # In use
    path('retrive-c/<int:id_pk>/<int:pk>/', ProductViewRetrive_C.as_view()), # In use
    path('product-search-c/', ProductViewSearch_C.as_view()), # In use
    
    path('product-create-a/', ProductViewCreate_A.as_view()), # admin
    path('list-a/', GetProductList_A.as_view()), # admin
    path('product-retrive-a/<int:id_pk>/', GetSingleProduct_A.as_view()), # admin
    path('product-update-a/<int:id_pk>/', ProductViewUpdate_A.as_view()), # admin

    # path('key-fetures-list-a/', KeyFeaturesViewList_A.as_view()),# admin
    # path('key-fetures-create-a/', KeyFeaturesViewCreate_A.as_view()),# admin
    # path('key-fetures-update-a/<int:id_pk>/', KeyFeaturesViewUpdate_A.as_view()),# admin
    # path('key-fetures-delete-a/<int:id_pk>/', KeyFeaturesViewDelete_A.as_view()),# admin

    path('media-post/', MediaView.as_view()), # admin
    
    # path('test/<int:id_pk>/', TestApi.as_view()) # Practice
    
]
